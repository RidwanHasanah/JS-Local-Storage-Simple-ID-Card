let name 	= document.getElementById('name'); /*Set variab;e for id html*/
let hp      = document.getElementById('hp');
let address = document.getElementById('address');
let photo 	= document.getElementById('photo');
let row 	= document.getElementById('row') 
var data = {"name":[],"hp":[],"address":[],"photo":[]} /*Set variable data empty in local storage*/

localData = JSON.parse(localStorage.getItem('data'));/*Get data JSON in localStorage and change data becomes JS Object*/
/*parse function make chage JSON data becomes a JavaScript object*/ 

if (localData != null) {
/*If data in local storage not null run data = localData*/
	data = localData;
}


btn.addEventListener('click', function(){
	if (name.value != "" && hp.value != "" && address.value != "" && photo.value != "") {
		/*If data input empty cant run function*/
		 data.name.unshift(name.value)
		 data.hp.unshift(hp.value)
		 data.address.unshift(address.value)
		 data.photo.unshift(photo.value)

		localStorage.setItem('data',JSON.stringify(data))
		show() /*Show Data after input data*/
		name.value="" /*Set input data become empty*/
		hp.value=""
		address.value=""
		photo.value=""
	}else {
		alert('I am Sory every field is "REQUIRED", please check your input field')
	}
})

function show(){
	let list = ''
	for (let i = 0; i < data.name.length; i++) {
		list +=`<div class="col col-md-4 col-lg-4 col-sm-6 col-xs-12 my-2">
				<div class="card bgc card-bgc" style="width: 18rem;">
				  <img class="card-img-top" src="${data.photo[i]}" alt="Created by Pondok Programmer">
				  <div class="card-body">
				    <h3 class="card-title py-1 px-1">${data.name[i]}</h3>
				  </div>
				  <ul class="list-group list-group-flush py-1 px-1">
				    <li class="list-group-item"><i class="fa fa-phone"></i> Phone : ${data.hp[i]}</li>
				    <li class="list-group-item"><i class="fa fa-address-card"></i> Address : ${data.address[i]}</li>
				  </ul>
				  <div class="card-body">
				  	<button onclick="remove(${i})" class="btn btn-outline-danger">Delete</button>
				  </div>
				</div>
			</div>`
	}
	row.innerHTML= list
}
show()

function remove(id){
	data.name.splice(id,1) //for remove data
	data.hp.splice(id,1)
	data.address.splice(id,1)
	data.photo.splice(id,1)

	// After remove data set data to localstorage
	localStorage.setItem('data', JSON.stringify(data))
	// then show data in local storage
	show()
}